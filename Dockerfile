FROM openjdk:11-jdk-stretch

MAINTAINER marwik@craftsmen.nl

RUN apt-get update -y && \
    apt-get install -y jq git curl graphviz software-properties-common && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y nodejs && \
    npm install -g @angular/cli